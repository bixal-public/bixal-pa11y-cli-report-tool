from setuptools import setup, find_packages

setup(
    name='bixal_pa11y',
    version='0.0.1-dev',
    py_modules = ['bixal_pa11y', 'app'],
    packages = find_packages(),
    entry_points={
        'console_scripts': [
            'bixal_pa11y=bixal_pa11y:cli'
        ]
    }
)
