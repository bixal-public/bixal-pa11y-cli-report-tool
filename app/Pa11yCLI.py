import csv
import json
import os
import webbrowser

from functools import partial
from http.server import HTTPServer, SimpleHTTPRequestHandler


class Pa11yCLI:
    """Builds CSV file from the Pa11y JSON report.
    """

    def __init__(self, config):
        """
        Set the initial state

        Args:
            config (dict): command line argument flags
        """
        self.config = config

    def buildJSONFromSitemap(self, pa11y_config):
        """Uses pa11y-ci node module to process sitemap.

        Args:
            pa11y_config (string): Points to the config file for Pa11y CLI tool.
        """
        pa11y_node_path = '/node_modules/pa11y-ci/bin/pa11y-ci.js'
        pa11y_cli_args = f"--config={self.config['root']}/{pa11y_config} --sitemap={self.config['sitemap']}"
        print("Processing the sitemap will take some time...")
        os.system(
            f"node {self.config['root']}{pa11y_node_path} {pa11y_cli_args}")

    def buildToCSV(self):
        """ Builds CSV report from json data source.
        """
        # # Open json file
        with open("./pa11y_results.json") as json_file:
            # get the data
            data = json.load(json_file)

            # Open csv file to write pa11y json results to.
            file = open('./pa11y_results.csv', 'w')
            cw = csv.writer(file)

            # Write the header row.
            cw.writerow([
                'url',
                'code',
                'message',
                'context',
                'selector'
            ])

            # Loop through rows and print values to new csv row.
            for result in data['results']:
                for data_array in data['results'][result]:
                    data_array['url'] = result
                    cw.writerow([
                        result,
                        data_array['code'],
                        data_array['message'],
                        data_array['context'],
                        data_array['selector'],
                    ])

            # Close the file stream.
            file.close()

    def serve(self):
        port = self.config['port']
        webbrowser.open(f"http://127.0.0.1:{port}", new=2)

        handler = partial(SimpleHTTPRequestHandler, directory="./web")
        httpd = HTTPServer(('localhost', port), handler)
        print(
            f"Serving Pa11y HTML Report on http://127.0.0.1:{port}"
        )
        httpd.serve_forever()
