<p align="center">
  <a href="" rel="noopener">
 <img width=120px height=24px src="https://bixal.com//static/e90efae7e816749d805876341ba16f9a/bixal-logo-full.svg" alt="Project logo"></a>
</p>

<h3 align="center">Bixal Pa11y</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![License](https://img.shields.io/badge/license-GNU-blue.svg)](/LICENSE)

</div>

---

<p align="center"> CLI Tool makes use of Pa11y-CI to create CSV, JSON, and HTML Reports.
    <br>
</p>

## 📝 Table of Contents

- [About](#about)
- [install](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

The application aims to help users get accessibility feedback using Pa11y.org CLI tools. It is a wrapper project that streamlines the configuration and creation of JSON, CSV, and HTML reports.

### Prerequisites

What things you need to install the software and how to install them.
<div style="color:red; font-weight: bold">
  Need Version Python 3.8+
</div>
```
python3 --version
```

Few optional ways to install python3
- [Download Python](https://www.python.org/downloads/)
- ```brew install python```
- ```sudo apt install python3```

<div style="color:red; font-weight: bold">
  Need Version Node 16+
</div>
```
node --version
```


You NVM to help manage and install Node. [Install NVM](https://github.com/nvm-sh/nvm)

### Installing

```
pip install git+https://gitlab.com/bixal-public/bixal-pa11y-cli-report-tool.git
```

## 🎈 Usage <a name="usage"></a>

<em>
  <strong>
    Pa11y is spelled using number one and not the letter L.
  </strong>
</em>

Recommendation for folder structure.
- Parent folder (can be clients name)
  - Unique name for the instance the report was created for (this is where you run th report)

<em>Example</em>
- bixalcom
  - 07222022 (run report in this folder)

List of flags currently avaliable in the project.
```
bixal_pa11y -h
```

To run all reports and start the html reports server using a sitemap
```
bixal_pa11y -s -w sitemap=https://domain.org
```

To serve an HTML report again. Be in the parent directory where the web result folder exist.
```
bixal_pa11y -s
```



## ⛏️ Built Using <a name = "built_using"></a>

- Python - CLI Framework
- Node - external libraries being used
  - [Pa11y-ci](https://github.com/pa11y/pa11y-ci) - Pa11y.org CLI library for scanning websites for accessibility issues.
  - [Pally-ci-html](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html) - HTML Report genereator.

## ✍️ Authors <a name = "authors"></a>

- [Clay Marshall](https://www.bixal.com/about/clay-marshall/) - Idea
- [Jeff Fortune](https://www.bixal.com/about/jeff-fortune/) - Initial work


See list of contributors
- Could be you

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- pa11y.org
