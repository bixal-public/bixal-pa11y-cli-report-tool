#!/usr/bin/env python3
import argparse
import os


from app.Pa11yCLI import Pa11yCLI


# Project root fot the baixal_pa11y application.
PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))


# Setup CLI arguments
parser = argparse.ArgumentParser(
    description='Help for Pally Json to CSV output',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter
)

# When web is flag will create a html report
parser.add_argument(
    '-w',
    '--web',
    action='store_true',
    help="Creates HTML reports.",
)

# Overrride the http server port number
parser.add_argument(
    '-p',
    '--port',
    default=53896,
    help="Set the port number for the site server (default:53896)"
)

# Flag for setting the start of a http server.
parser.add_argument(
    '-s',
    '--serve',
    action='store_true',
    help="Simple python http server to render html results."
)

# Sitemap argument takes a URL to a XML sitemap
parser.add_argument(
    '--sitemap',
    default=None,
    help="Argument to set sitemap url",
)


def cli():
    """Handles the CLI request for the applicaiton.
    """
    # Grab the inbound arguments and set config.
    args = parser.parse_args()
    config = vars(args)
    config['root'] = PROJECT_ROOT

    # Declare instance of app.Pa11yCLI class.
    pa11yCLI = Pa11yCLI(config)

    # Process pa11y request using sitemap if it set.
    if config['sitemap']:
        # Set the config file based on flag web beign set.
        pa11y_config = 'config.json'
        if config['web']:
            pa11y_config = 'config-html.json'
        # Get JSON and HTML reports
        pa11yCLI.buildJSONFromSitemap(pa11y_config)
        # Build CSV From
        pa11yCLI.buildToCSV()

    if config['serve']:
        pa11yCLI.serve()
